# job-images

This project maintains our macOS images to be used with [nesting](https://gitlab.com/gitlab-org/fleeting/nesting) on our macOS Runners in AWS (starting with macOS 13 Ventura).
This project can also be used by self-managed customers to build their own macOS runner machines.

Specifically, the images are built to run as virtual machines on a macos host using the [macOS Virtualization Framework](https://developer.apple.com/documentation/virtualization).
They can only be built and then run on a macOS host.

The code is organized as follows:

* `ansible/` Ansible configuration and playbooks.
* `packer/` macOS version specific Packer configuration and build scripts.
* `tests/` tests ensuring expected configurations.
* `toolchain/` job image configuration definitions.

## How it works

This project uses [Packer](https://www.packer.io/), [Ansible](https://www.ansible.com/), and [Tart](https://tart.run/) to create a macOS machine image from an [IPSW](https://en.wikipedia.org/wiki/IPSW) file directly from Apple. The generated machine image is optimized to be run as a GitLab Runner host, as well as a virtual machine that can be run on self-managed hardware and connected to self-managed or SaaS GitLab projects.

## What's included?

Aside from XCode and Command Line Tools, this project includes various tools defined in the `toolchain` directory.

Check the current [Macos Hosted inventories](https://gitlab-org.gitlab.io/ci-cd/shared-runners/images/macos-image-inventory/) for a summary.

## How to build an image

GitLab builds the images via the `.gitlab/ci` definitions using a dedicated macOS runner.
See the [macos-build-machine README](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macos-build-machine/-/blob/main/README.md?ref_type=heads) for more details.

### Prerequisites

To build and run these virtual machines, you'll need to have [Packer](https://www.packer.io/), [Ansible](https://www.ansible.com/), and [Tart](https://tart.run/) installed on the host machine. They can all be installed via Homebrew:
 
```
brew install packer ansible cirruslabs/cli/tart
```

You'll also need to install the Tart plugin for Packer. This can be done by running:

```
packer plugins install github.com/cirruslabs/tart
```

### Base Images

Three base images are required for this build process:

1. A macOS base image
1. An Xcode distribution
1. Command Line Tools for Xcode

Apple provides downloadable base images and packages. You can find Xcode and Command Line Tools for Xcode in the Apple Developer Portal under [Software Downloads](Software Downloads). 

[Apple Wiki](https://theapplewiki.com/wiki/Firmware/Mac) and [IPSW.me](https://ipsw.me/) both maintain links to macOS base images distributed by Apple.

The location of these files can be configured in the `packer/conf/macos-{version}.pkrvars.hcl` and `packer/conf/xcode-{version}.pkrvars.hcl` files.

## Build a Machine Image

1. Start by building a base image based on an IPSW supplied by Apple. This step automates the standard onboarding flow of a new macOS machine. This process takes some time, so we'll create a snapshot once done and run the subsequent steps from the snapshot.
    
    ```
    $ packer build -var-file="packer/conf/macos-15.pkrvars.hcl" packer/templates/macos-15-base.pkr.hcl
    ```

1. Next, run the `toolchain` template to install all development tools. This will include Homebrew, ruby, python, go, etc.

    ```
    $ packer build -var-file="packer/conf/macos-15.pkrvars.hcl" packer/templates/toolchain.pkr.hcl
    ```

1. Next, run the `xcode` template to install the version of Xcode for the image.

    ```
    $ packer build -var-file="packer/conf/xcode-16.pkrvars.hcl" -var-file="packer/conf/macos-15.pkrvars.hcl" packer/templates/xcode.pkr.hcl
    ```
    
    The result of this step will be a machine image you can run in Tart called `macos-15-xcode-16`.
    
## Self Hosting Runner
    
1. The images created by this project can be used to run your own self-hosted runners. To do this, start by booting up one of the images with Tart. For example:

    ```sh
	tart run macos-15-xcode-16
	```

1. SSH to the virtual machine, and register the GitLab Runner. Note: the ssh password is `gitlab`.
    
    ```sh
    ssh gitlab@$(tart ip macos-15-xcode-16)
    ```

1. Create a new runner registration in your projects CI/CD settings. This process will generate a runner registration token (`YOUR_TOKEN`) to be used later. Please see [the instructions](https://docs.gitlab.com/runner/register/?tab=macOS) for additional details and configuration options for registering a GitLab runner.

	From your SSH session, run:
	
    ```sh
    gitlab-runner register -n --url YOUR_HOST --executor shell --shell bash --token YOUR_TOKEN
    ```
        
    Then start the runner by running:

    ```sh
    gitlab-runner run
    ```

## Testing

Tests are included to ensure the all provisioned software is installed and functioning correctly. Test can be run on the host machine against any built virtual machine. `sshpass` is required to run the tests and can be installed via Homebrew:

```sh
brew tap gitlab/shared-runners https://gitlab.com/gitlab-org/ci-cd/shared-runners/homebrew.git
brew install gitlab/shared-runners/sshpass
```

To run the tests, boot up the virtual machine with (`macos-15-toolchain` is the image in this example):

```sh
tart run macos-15-toolchain
```

Then run:

```sh
TARGET_HOST=$(tart ip macos-15-toolchain) TARGET_PORT=22 LOGIN_PASSWORD=gitlab bundle exec rspec spec
```
