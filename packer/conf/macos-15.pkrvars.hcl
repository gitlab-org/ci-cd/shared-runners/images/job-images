// ipsw_source = "https://updates.cdn-apple.com/2024FallFCS/fullrestores/072-44245/E811A1B0-28A9-4FCD-AE32-322E796F0EB8/UniversalMac_15.2_24C101_Restore.ipsw"
ipsw_source               = "/Volumes/base_images/UniversalMac_15.2_24C101_Restore.ipsw"
command_line_tools_source = "/Volumes/base_images/Command_Line_Tools_for_Xcode_16.2.dmg"
macos_version_name        = "macos-15"
macos_version_number      = "15.2.0"
