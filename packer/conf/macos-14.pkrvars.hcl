// ipsw_source = "https://updates.cdn-apple.com/2024SummerFCS/fullrestores/062-52859/932E0A8F-6644-4759-82DA-F8FA8DEA806A/UniversalMac_14.6.1_23G93_Restore.ipsw"
ipsw_source               = "/Volumes/base_images/UniversalMac_14.6.1_23G93_Restore.ipsw"
command_line_tools_source = "/Volumes/base_images/Command_Line_Tools_for_Xcode_15.3.dmg"
macos_version_name        = "macos-14"
macos_version_number      = "14.6.1"
