#!/bin/bash -e -o pipefail

set -eo pipefail

# check that we have at least arg1 GiB of free space on the macos root disk
# and at most arg2 GiB of free space

if [ -z "$1" ] || [ -z "$2" ]; then
    echo "Usage: check-diskspace.sh <minGiB> <maxGiB>"
    exit 1 
fi

echo "Human readable free disk space:"
df -h /

echo "Checking free disk space between ${1}GiB and ${2}GiB"

# get the size of the root disk
# -g is a macos specific option (doesn't work on linux)
root_disk_size=$(df -g / | tail -1 | awk '{print $4}')

echo "Root disk free space: ${root_disk_size}GiB"

if [ "$root_disk_size" -lt "$1" ]; then
    echo "Not enough free space on root disk"
    exit 1  
fi

if [ "$root_disk_size" -gt "$2" ]; then
    echo "Too much free space on root disk"
    exit 1  
fi
