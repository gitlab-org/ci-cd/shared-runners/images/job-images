packer {
  required_plugins {
    tart = {
      version = ">= 1.2.0"
      source  = "github.com/cirruslabs/tart"
    }
  }
}

variable "macos_version_name" {
  type = string
}

variable "xcode_source" {
  type = string
}

variable "xcode_version" {
  type = string
}

variable "xcode_major_version" {
  type = string
}

source "tart-cli" "tart" {
  vm_base_name = "${var.macos_version_name}-xcode-${var.xcode_major_version}"
  vm_name      = "${var.macos_version_name}-xcode-${var.xcode_major_version}"
  ssh_password = "gitlab"
  ssh_username = "gitlab"
  ssh_timeout  = "120s"
}

build {
  sources = ["source.tart-cli.tart"]

  provisioner "shell" {
    inline = [
      "echo \"----------------------------------------------------\"",
      "echo \"macOS Version\"",
      "sw_vers -productVersion",
      "echo \"----------------------------------------------------\"",
      "echo \"Xcode Version\"",
      "xcodebuild -version",
      "echo \"----------------------------------------------------\"",
      "echo \"Xcode Command Line Tools Version\"",
      "pkgutil --pkg-info=com.apple.pkg.CLTools_Executables | grep version",
      "echo \"----------------------------------------------------\"",
      "echo \"Apple Swift Version\"",
      "swift --version",
      "echo \"----------------------------------------------------\"",
      "echo \"Preinstalled Homebrew Versions\"",
      "brew list --version",
      "echo \"----------------------------------------------------\"",
      "echo \"Preinstalled Asdf Version\"",
      "asdf list",
      "echo \"----------------------------------------------------\"",
      "echo \"Flutter Version\"",
      "flutter --version",
      "echo \"----------------------------------------------------\"",
      "echo \"GitLab Runner Version\"",
      "gitlab-runner -v",
      "echo \"----------------------------------------------------\"",
      "echo \"Fastlane Version\"",
      "fastlane -v",
      "echo \"----------------------------------------------------\"",
      "echo \"Ruby Gems\"",
      "gem list",
      "echo \"----------------------------------------------------\"",
      "echo \"Python Packages\"",
      "gem list",
      "echo \"----------------------------------------------------\"",
    ]
  }
}


