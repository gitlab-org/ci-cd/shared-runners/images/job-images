# frozen_string_literal: true

require 'spec_helper'

context 'base system' do
  # check that we set the system timezone to GMT / UTC
  describe command('sudo systemsetup -gettimezone') do
    it { should be_a_successful_cmd }
    its(:stdout) { should match(/Time Zone: GMT/) }
  end

  # ensure Xcode command line tools are installed
  describe file('/Library/Developer/CommandLineTools/usr/bin/git') do
    it { should be_file }

    it 'is able to clone repositories' do
      with_tmpdir do |tmpdir|
        cmd = command("#{subject.name} clone https://github.com/mizzy/serverspec.git #{tmpdir}")
        expect(cmd).to be_a_successful_cmd
      end
    end
  end

  # ensure Ansible is installed
  describe command('which ansible') do
    it { should be_a_successful_cmd }
    its(:stdout) { should match(/\/Users\/gitlab\/Library\/Python\/3.9\/bin\/ansible/) }
  end
end
