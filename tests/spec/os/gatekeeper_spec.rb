# frozen_string_literal: true

require 'spec_helper'

# gatekeeper cannot be disabled with spctl on macOS 15
context 'gatekeeper', skip: macos_version >= '15'  do
  # check that gatekeeper is disabled
  describe command('spctl --status') do
    its(:stdout) { should match(/assessments disabled/) }
  end
end
