PHONY: upload-to-s3
upload-to-s3:
	if [ -z "$$VM_NAME" ]; then \
	  echo "Error: VM_NAME environment variable is not set."; \
	  exit 1; \
	fi; \
	DIR=/Users/ec2-user/.tart/vms/$$VM_NAME; \
	DATE=$$(date -u +"%Y-%m-%dT%H:%M:%SZ"); \
	FILENAME=$${VM_NAME}_$${VERSION}_$${DATE}.tar.zst; \
	TAGSET='{"TagSet": [\
		{"Key": "GitlabPipelineID", "Value": "'"$(CI_PIPELINE_ID)"'" }, \
		{"Key": "GitlabPipelineURL", "Value": "'"$(CI_PIPELINE_URL)"'" }, \
		{"Key": "GitlabJobID", "Value": "'"$(CI_JOB_ID)"'" }, \
		{"Key": "GitlabJobURL", "Value": "'"$(CI_JOB_URL)"'" }, \
		{"Key": "GitlabCommitRefName", "Value": "'"$(CI_COMMIT_REF_NAME)"'" }, \
		{"Key": "GitlabCommitSHA", "Value": "'"$(CI_COMMIT_SHA)"'" } \
		] }'; \
	echo "Creating tarball with filename: $$FILENAME"; \
	tar -c -C $$DIR . | \
		zstd -T0 | \
		aws s3 cp - "s3://gitlab-macos-runner-job-images/$$FILENAME"; \
	aws s3api put-object-tagging \
		--bucket gitlab-macos-runner-job-images \
		--key $$FILENAME \
		--tagging "$$TAGSET"
